import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import AttendeeForm from './AttendeesList'
import ConferenceForm from './ConferenceForm';
import LocationForm from './LocationForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} />
              <Route path="attendees/new" element={<AttendConferenceForm />} />
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="/presentations/new" element={<PresentationForm />} />
          <Route index element={<MainPage />} />

        </Routes>
      </div>
    </BrowserRouter>
  );

}

export default App;
