function createCard(title, description, pictureUrl, starts, ends, location) {
    return `
      <div class="card shadow mb-4">
        <img class="card-img-top" src="${pictureUrl}" alt="${title}">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class ="card-footer"><time>${starts} - ${ends}</time></div>
      </div>
    `;
  }
  function alert(){
    return `
    <div class = "container">
      <div class="alert alert-danger" role="alert">
        <h5>This is an error!<h5>
        <p>
            ===ERROR=== SELF DESTRUCT
        </p>
      </div>
    </div>
    `;

  }


  window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Response not ok')
      } else {
        const data = await response.json();
        const columnCount = 3;
        let columnIndex = 0;

        for (let i = 0; i < data.conferences.length; i++) {
          const conference = data.conferences[i];
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);

          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const starts = new Date(details.conference.starts).toLocaleDateString();
            const ends = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const html = createCard(name, description, pictureUrl, starts, ends, location);
            const column = document.querySelectorAll('.col')[columnIndex];
            column.innerHTML += html;
            columnIndex = (columnIndex + 1) % columnCount;
          }
        }
        ///check if logged in
        const loggedIn = document.cookie.includes('loggedIn=true')
        const navLinks = document.querySelectorAll('.nav-link');
        if(loggedIn) {
          navLinks[1].classList.remove('d-none');
          navLinks[2].classList.remove('d-none');
        }
      }
    } catch (e) {
      console.error('error', e)
      const errorAlert = alert();
      const container = document.querySelector('.container');
      container.insertAdjacentHTML('afterbegin', errorAlert);
    }
  });
