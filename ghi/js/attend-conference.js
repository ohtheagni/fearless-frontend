window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      // Here, add the 'd-none' class to the loading icon
      const loadingIcon = document.getElementById('loading-conference-spinner');
      loadingIcon.classList.add('d-none');

      // Here, remove the 'd-none' class from the select tag
      selectTag.classList.remove('d-none');
    }


    const attendeeForm = document.getElementById('create-attendee-form'); // Get the attendee form element by its id
    attendeeForm.addEventListener('submit', async (event) => { // Add an event handler for the submit event
    event.preventDefault(); // Prevent the default from happening
    const formData = new FormData(attendeeForm); // Create a FormData object from the form
    const json = JSON.stringify(Object.fromEntries(formData)); // Get a new object from the form data's entries
    const options = { // Create options for the fetch
        method: "post",
        body: json,
        headers: {
        'Content-Type': 'application/json',
        },
    };
    const url = 'http://localhost:8001/api/attendees/'; // Make the fetch using the await keyword to the URL
    const response = await fetch(url, options);
    if (response.ok) {
        attendeeForm.reset();
        const newConference = await response.json();
        const successAlert = document.getElementById('success-message'); // When the response from the fetch is good
        const attendeeForm = document.getElementById('create-attendee-form');

        successAlert.classList.remove('d-none'); //remove the d-none from the success alert and add d-none to the form.
        attendeeForm.classList.add('d-none');
    }
    });

  });
