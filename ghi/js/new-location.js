window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';
    const stateResponse = await fetch(url);

    if (stateResponse.ok) {
      const stateData = await stateResponse.json();
        const selectStateTag = document.getElementById('state');// Get the select tag element by its id 'state'
        for (let state of stateData.states) {  // For each state in the states property of the data
            const option = document.createElement('option') //Create an 'option' element
            option.value = state.abbreviation; //Set the .value property of the option element to the states abbreviation
            option.innerHTML = state.name; //Set the .innerHTML property of the element to the states name
            selectStateTag.appendChild(option); //Append the option element as a child of the select tag
        }
    }
    //Handle form submission
    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const locationURL = 'http://localhost:8000/api/locations/';
        const locationResponse = await fetch(locationURL, fetchConfig);
        if (locationResponse.ok) {
            formTag.reset();
            const newLocation = await locationResponse.json();
        }
    })




});
