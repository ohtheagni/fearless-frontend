window.addEventListener('DOMContentLoaded', async () => {
    // Load available locations
    const locationsUrl = 'http://localhost:8000/api/locations/';
    const locationsResponse = await fetch(locationsUrl);

    if (locationsResponse.ok) {
      const locationsData = await locationsResponse.json();
      const locationSelectTag = document.getElementById('location'); //Get locationSelectTag element by its id 'location'
      for (let location of locationsData.locations) { //for each location in locations property of the data
        const option = document.createElement('option'); //create an option element
        option.value = location.id; //set the value of the option element to the locations id
        option.innerHTML = location.name; //set the innerHTML property of the element to the locations name
        locationSelectTag.appendChild(option); //Append teh option element as a child of the locations tag
      }
    }

    // Handle form submission
    const form = document.getElementById('create-conference-form');
    form.addEventListener('submit', async (event) => {
      event.preventDefault();
      const formData = new FormData(form);
      const dataObject = Object.fromEntries(formData)
      const json = JSON.stringify(Object.fromEntries(formData));

    //   const conferenceData = {
    //     name: formData.get('name'),
    //     starts: formData.get('starts'),
    //     ends: formData.get('ends'),
    //     description: formData.get('description'),
    //     max_presentations: Number(formData.get('max_presentations')),
    //     max_attendees: Number(formData.get('max_attendees')),
    //     location: Number(formData.get('location'))
    //   };

      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
            'Content-Type': 'applications/json',
        },
      };
      console.log(fetchConfig)
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        form.reset();
        const newConference = await response.json();
        alert('Conference created successfully!');
      } else {
        alert('Failed to create conference.');
      }
    });
  });
